package com.legacy.no_nitwits;

import java.util.logging.Logger;

import net.minecraft.entity.merchant.villager.VillagerEntity;
import net.minecraft.entity.merchant.villager.VillagerProfession;
import net.minecraft.entity.monster.ZombieVillagerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod(NoNitwitsMod.MODID)
public class NoNitwitsMod
{
	public static final String NAME = "No Nitwits";
	public static final String MODID = "no_nitwits";
	public static Logger LOGGER = Logger.getLogger(MODID);

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MODID, name);
	}

	public static String find(String name)
	{
		return MODID + ":" + name;
	}

	public NoNitwitsMod()
	{
		MinecraftForge.EVENT_BUS.register(new EntityEvents());
	}

	class EntityEvents
	{
		@SubscribeEvent
		public void onEntitySpawn(EntityJoinWorldEvent event)
		{
			if (event.getEntity() instanceof ZombieVillagerEntity)
			{
				ZombieVillagerEntity villager = (ZombieVillagerEntity) event.getEntity();

				if (villager.getVillagerData().getProfession() == VillagerProfession.NITWIT)
				{
					villager.func_213792_a(villager.getVillagerData().withProfession(VillagerProfession.NONE));
				}
			}

			if (event.getEntity() instanceof VillagerEntity)
			{
				VillagerEntity villager = (VillagerEntity) event.getEntity();

				if (villager.getVillagerData().getProfession() == VillagerProfession.NITWIT)
				{
					villager.setVillagerData(villager.getVillagerData().withProfession(VillagerProfession.NONE));
				}
			}
		}

		// Might end up adding this if something goes wrong as a failsafe.
		/*@SubscribeEvent 
		public void onEntityUpdate(LivingUpdateEvent event)
		{
			if (event.getEntity() instanceof VillagerEntity)
			{
				VillagerEntity villager = (VillagerEntity) event.getEntity();
			
				if (villager.getVillagerData().getProfession() == VillagerProfession.NITWIT)
				{
					villager.setVillagerData(villager.getVillagerData().withProfession(VillagerProfession.NONE));
				}
			}
		}*/
	}
}
